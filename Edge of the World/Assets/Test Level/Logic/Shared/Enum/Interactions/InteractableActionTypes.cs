﻿public enum InteractableActionTypes
{
    SitDown,
    GetUp,
    Sleep,
    WakeUp,
    Take,
    Open,
    Push,
    Pull,
    Climb,
    Talk,
    Leave
}