﻿public enum DebugEventTypes
{
    ExitGame,
    ShowSystemInfo,
    ShowTools,
    RespawnItems
}