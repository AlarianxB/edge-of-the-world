﻿using UnityEngine;

public interface ICharacterSoundGroup
{
    public float Volume { get; }
    public AudioClip[] Sounds { get; }
    public CharacterSoundTypes Name { get; }
}