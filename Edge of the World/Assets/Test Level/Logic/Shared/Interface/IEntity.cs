﻿using UnityEngine;

public interface IEntity
{
    public GameObject GameObjectRef { get; }
    public Collider Collider { get; }
    public Transform Transform { get; }
    public Renderer Renderer { get; }
}