﻿using UnityEngine;

public interface IInteractableObject : IEntity, IInteractable
{
    
    public Animator Animator { get; }
    public Vector3 HandPosition { get; }
    public Vector3 HandRotation { get; }
    public GameObject VisualMarker { get; set; }
    public Vector3 VisualMarkerPositionOffset { get; set; }
    public bool CanShowVisualMarker { set; get; }
    public Vector2 CharacterPositionOffset { get; }
    public InteractableObjectTypes ObjectType { get; }
    public float HighlightSize { get; }
    public SpriteRenderer VisualMarkerSpriteRenderer { get; set; }
}