﻿using UnityEngine;

public static class GameUtils
{
    public static ICharacter GetPlayerEntity()
    {
        return GameObject.FindWithTag("Player").GetComponent<IController>().GetEntity<ICharacter>();
    }
}
