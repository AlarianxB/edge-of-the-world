﻿using UnityEngine;

[CreateAssetMenu(fileName = "Interactable Category", menuName = "Interactable/Interactable Category", order = 0)]
public class InteractableCategoryTemplate : ScriptableObject
{
    public InteractableObjectTypes objectType;
    public float triggerZoneRadius;
    public float maxDistanceToPlayer;
    public float visualMarkerSize;
}