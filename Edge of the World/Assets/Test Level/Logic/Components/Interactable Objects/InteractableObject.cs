﻿using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableObject : IInteractableObject
{
    private static readonly Dictionary<InteractableActionTypes, string> Actions = new Dictionary<InteractableActionTypes, string>
    {
        {InteractableActionTypes.Take, "TAKE"},
        {InteractableActionTypes.Sleep, "SLEEP"},
        {InteractableActionTypes.WakeUp, "WAKE_UP"},
        {InteractableActionTypes.GetUp, "GET_UP"},
        {InteractableActionTypes.SitDown, "SIT_DOWN"},
        {InteractableActionTypes.Open, "OPEN"},
        {InteractableActionTypes.Climb, "CLIMB"},
        {InteractableActionTypes.Push, "PUSH"},
        {InteractableActionTypes.Pull, "PULL"}
    }; //TODO move this in utils

    private InteractableActionTypes currentActionType;

    protected InteractableObject(GameObject gameObjectRef, InteractableCategoryTemplate interactableCategoryTemplate,
        InteractableObjectTemplate objectTemplate, Vector3 handPosition, Vector3 handRotation)
    {
        GameObjectRef = gameObjectRef;
        Transform = GameObjectRef.GetComponent<Transform>();
        Animator = GameObjectRef.GetComponent<Animator>();
        Renderer = GameObjectRef.GetComponent<Renderer>();
        Collider = GameObjectRef.GetComponent<Collider>();

        ObjectCenter = Renderer.bounds.center;
        TriggerZoneRadius = interactableCategoryTemplate.triggerZoneRadius;
        CharacterPositionOffset = objectTemplate.characterPositionOffset;
        ObjectType = interactableCategoryTemplate.objectType;
        HighlightSize = objectTemplate.highlightSize;
        VisualMarkerPositionOffset = objectTemplate.visualMarkerPositionOffset;
        CanInteractWithPlayer = false;
        CanShowVisualMarker = false;
        HandPosition = handPosition;
        HandRotation = handRotation;
        LocalizationUtils.Translate("InteractableObjects", objectTemplate.objectName, translation => Name = translation);

        switch (interactableCategoryTemplate.objectType)
        {
            case InteractableObjectTypes.GroundItem:
            case InteractableObjectTypes.SurfaceItem:
                CurrentActionType = InteractableActionTypes.Take;
                break;
            case InteractableObjectTypes.Bed:
                CurrentActionType = InteractableActionTypes.Sleep;
                break;
            case InteractableObjectTypes.Bench:
                CurrentActionType = InteractableActionTypes.SitDown;
                break;
            case InteractableObjectTypes.Door:
                CurrentActionType = InteractableActionTypes.Open;
                break;
            case InteractableObjectTypes.Ladder:
                CurrentActionType = InteractableActionTypes.Climb;
                break;
            case InteractableObjectTypes.MovableObject:
                CurrentActionType = InteractableActionTypes.Push;
                break;
        }
    }

    public string Name { get; set; }
    public GameObject GameObjectRef { get; }
    public Transform Transform { get; }
    public Animator Animator { get; }
    public Collider Collider { get; }
    public Renderer Renderer { get; }
    public Vector3 ObjectCenter { get; }
    public GameObject VisualMarker { get; set; }
    public Vector3 VisualMarkerPositionOffset { get; set; }
    public Vector3 HandPosition { get; }
    public Vector3 HandRotation { get; }
    public bool CanInteractWithPlayer { get; set; }
    public bool CanShowOutline { get; set; }
    public bool CanShowVisualMarker { get; set; }
    public Collider TriggerAreaCollider { get; set; }
    public Vector2 CharacterPositionOffset { get; }
    public bool IsInteracting { get; set; }
    public float DistanceToPlayer { get; set; }
    public InteractableObjectTypes ObjectType { get; }

    public InteractableActionTypes CurrentActionType
    {
        set
        {
            currentActionType = value;
            LocalizationUtils.Translate("InteractableActions", Actions[value], translation => CurrentActionName = translation);
        }

        get => currentActionType;
    }

    public string CurrentActionName { get; private set; }
    public float TriggerZoneRadius { get; }
    public float HighlightSize { get; }
    public SpriteRenderer VisualMarkerSpriteRenderer { get; set; }
}