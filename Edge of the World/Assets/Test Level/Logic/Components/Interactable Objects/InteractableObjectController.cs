﻿using UnityEngine;

public abstract class InteractableObjectController : MonoBehaviour, IController
{
    private static readonly int Highlight = Shader.PropertyToID("Highlight");
    private static readonly int HighlightSizeProperty = Shader.PropertyToID("HighlightSize");

    public InteractableCategoryTemplate categoryTemplate;
    public InteractableObjectTemplate objectTemplate;
    public Vector3 handPosition;
    public Vector3 handRotation;

    protected IInteractableObject interactableObjectEntity;
    private Camera playerCamera;
    protected ICharacter playerEntity;

    protected virtual void Start()
    {
        CharacterEnvironmentInteractionService.Event += OnInteractionEvent;

        interactableObjectEntity = GetInteractableObjectInstance();
        playerEntity = GameUtils.GetPlayerEntity();
        playerCamera = CameraUtils.GetPlayerCamera();
        GenerateTriggerArea();
        CreateVisualMarker();
    }

    protected virtual void Update()
    {
        SetCanInteract();
        SetDistanceToPlayer();
        UpdateVisualMarker();
        UpdateOutline();
        // UpdateHighlight();
        //TODO we disable the highlight until we decide what to do with it
    }

    private void OnDestroy()
    {
        CharacterEnvironmentInteractionService.Event -= OnInteractionEvent;
        Destroy(interactableObjectEntity.VisualMarker);
    }

    public T GetEntity<T>()
    {
        return (T) interactableObjectEntity;
    }

    protected void SetVisualMarkerPosition(bool setOnTheOtherSide = false)
    {
        Bounds interactableObjectBounds = interactableObjectEntity.Renderer.bounds;
        Vector3 visualMarkerPosition = interactableObjectEntity.ObjectCenter;
        visualMarkerPosition = new Vector3(visualMarkerPosition.x, interactableObjectBounds.size.y / 2 + visualMarkerPosition.y + 0.2f,
            visualMarkerPosition.z);

        Vector3 visualMarkerPositionOffset = GetVisualMarkerOffset();

        if (setOnTheOtherSide) visualMarkerPositionOffset.z *= -1;
        visualMarkerPosition = interactableObjectEntity.Transform.InverseTransformPoint(visualMarkerPosition);
        visualMarkerPosition += visualMarkerPositionOffset;
        interactableObjectEntity.VisualMarker.transform.localPosition = visualMarkerPosition;
    }

    private void OnInteractionEvent(InteractionEventTypes eventType, ICharacter character, IInteractable interactable = null)
    {
        if (!(interactable is IInteractableObject interactableObject) ||
            interactableObject.GameObjectRef.GetInstanceID() != gameObject.GetInstanceID()) return;

        switch (eventType)
        {
            case InteractionEventTypes.CharacterEnter:
                interactableObjectEntity.CanShowVisualMarker = true;
                CharacterEnvironmentInteractionService.EmitterStack.Add(interactableObject);
                break;
            case InteractionEventTypes.CharacterExit:
                interactableObjectEntity.CanShowVisualMarker = false;
                CharacterEnvironmentInteractionService.EmitterStack.Remove(interactableObject);
                break;
            default:
                return;
        }
    }

    private void SetDistanceToPlayer()
    {
        interactableObjectEntity.DistanceToPlayer =
            Vector3.Distance(interactableObjectEntity.TriggerAreaCollider.transform.position, playerEntity.Transform.position);
    }

    private void SetCanInteract()
    {
        interactableObjectEntity.CanInteractWithPlayer = interactableObjectEntity.DistanceToPlayer < categoryTemplate.maxDistanceToPlayer;
    }

    private void GenerateTriggerArea()
    {
        GameObject triggerZone = new GameObject {name = "Trigger Zone"};
        triggerZone.transform.parent = gameObject.transform;
        triggerZone.transform.position = interactableObjectEntity.ObjectCenter;
        triggerZone.layer = 2; // Ignore Raycasts

        interactableObjectEntity.TriggerAreaCollider = (SphereCollider) triggerZone.AddComponent(typeof(SphereCollider));
        ObjectTriggerZoneController objectTriggerZoneController =
            (ObjectTriggerZoneController) triggerZone.AddComponent(typeof(ObjectTriggerZoneController));
        if (interactableObjectEntity.TriggerAreaCollider is null) return;

        ((SphereCollider) interactableObjectEntity.TriggerAreaCollider).radius = interactableObjectEntity.TriggerZoneRadius;
        interactableObjectEntity.TriggerAreaCollider.isTrigger = true;
    }

    private void CreateVisualMarker()
    {
        GameObject visualMarkerRef = GameObject.FindWithTag("VisualMarker"); // TODO use addressable
        GameObject visualMarker = Instantiate(visualMarkerRef, interactableObjectEntity.Transform.position, Quaternion.identity,
            interactableObjectEntity.Transform);
        interactableObjectEntity.VisualMarker = visualMarker;
        interactableObjectEntity.VisualMarkerSpriteRenderer = interactableObjectEntity.VisualMarker.GetComponent<SpriteRenderer>();
        SetVisualMarkerPosition();
    }

    private void UpdateVisualMarker()
    {
        Vector3 visualMarkerPosition = interactableObjectEntity.VisualMarker.transform.position;
        Vector3 playerCameraPosition = playerCamera.transform.position;
        float distanceToPlayerCamera = (playerCameraPosition - visualMarkerPosition).magnitude;
        float visualMarkerScale = distanceToPlayerCamera * categoryTemplate.visualMarkerSize * playerCamera.fieldOfView;

        interactableObjectEntity.VisualMarker.transform.localScale = Vector3.one * visualMarkerScale;
        interactableObjectEntity.VisualMarker.transform.forward = visualMarkerPosition - playerCameraPosition;
        interactableObjectEntity.VisualMarkerSpriteRenderer.enabled =
            !interactableObjectEntity.CanInteractWithPlayer && interactableObjectEntity.CanShowVisualMarker;
    }

    private void UpdateHighlight()
    {
        interactableObjectEntity.Renderer.material.SetInt(Highlight,
            interactableObjectEntity.CanInteractWithPlayer && !((IPlayer) playerEntity).IsInteracting ? 1 : 0);
        interactableObjectEntity.Renderer.material.SetFloat(HighlightSizeProperty, interactableObjectEntity.HighlightSize);
    }

    protected virtual void UpdateOutline()
    {
        //TODO Fix Outline for door knobs
        SetLayer(interactableObjectEntity.CanInteractWithPlayer && !((IPlayer) playerEntity).IsInteracting ? 13 : 0);
    }

    protected void SetLayer(int layerId, bool onlyChildren = false)
    {
        //TODO add this in utils
        //TODO only use SetLayer children for door

        int childCount = interactableObjectEntity.GameObjectRef.transform.childCount;


        if (childCount == 0) return;
        for (int i = 0; i < childCount; i++)
        {
            GameObject child = interactableObjectEntity.GameObjectRef.transform.GetChild(i).gameObject;
            if (child.layer == 2)
                continue;
            child.layer = layerId;
        }

        if (onlyChildren) return;
        interactableObjectEntity.GameObjectRef.layer = layerId;
    }


    protected abstract IInteractableObject GetInteractableObjectInstance();

    protected virtual Vector3 GetVisualMarkerOffset()
    {
        return interactableObjectEntity.VisualMarkerPositionOffset;
    }
}