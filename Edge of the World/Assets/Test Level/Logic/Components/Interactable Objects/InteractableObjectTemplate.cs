﻿using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "Interactable Object", menuName = "Interactable/Interactable Object", order = 1)]
public class InteractableObjectTemplate : ScriptableObject
{
    public string objectName;
    public float highlightSize;

    [FormerlySerializedAs("playerPositionOffset")]
    public Vector2 characterPositionOffset;
    public Vector3 visualMarkerPositionOffset;
    //TODO make an array of properties for this template and category template. this way we can choose which properties to have for every template
}