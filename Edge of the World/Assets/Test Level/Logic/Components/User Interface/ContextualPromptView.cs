﻿using UnityEngine;
using UnityEngine.UI;

public class ContextualPromptView
{
    public ContextualPromptView(GameObject container, UserInterfaceTextTemplate contextualPromptViewTextTemplate)
    {
        Container = container;
        RectTransform containerTransform = Container.GetComponent<RectTransform>();

        ObjectName = UserInterfaceUtils.CreateUIGameObject<Text>("ObjectType", containerTransform, new Vector3(91.6f, -42.8f, 0),
            new Vector2(260.5778f, 55.47014f));
        ActionType = UserInterfaceUtils.CreateUIGameObject<Text>("ActionType", containerTransform, new Vector3(48.5f, 12.7f, 0),
            new Vector2(260.5778f, 55.47014f));
        ActionKey = UserInterfaceUtils.CreateUIGameObject<Image>("ActionKey", containerTransform, new Vector3(205.2f, 12.7f, 0),
            new Vector2(33.32721f, 55.47011f));
        ActionKey.preserveAspect = true;

        SetUITextProperties(ObjectName, contextualPromptViewTextTemplate);
        SetUITextProperties(ActionType, contextualPromptViewTextTemplate);
    }
    
    public static void SetUITextProperties(Text textElement, UserInterfaceTextTemplate textTemplate)
    {
        textElement.font = textTemplate.font;
        textElement.fontStyle = textTemplate.fontStyle;
        textElement.fontSize = textTemplate.fontSize;
        textElement.lineSpacing = textTemplate.lineSpacing;
        textElement.alignment = textTemplate.alignment;
        textElement.color = textTemplate.color;
        textElement.resizeTextMinSize = textTemplate.fontMinSize;
        textElement.resizeTextMaxSize = textTemplate.fontMaxSize;
        textElement.resizeTextForBestFit = true;
    }

    public GameObject Container { get; }
    public Image ActionKey { get; }
    public Text ActionType { get; }

    public Text ObjectName { get; }
}