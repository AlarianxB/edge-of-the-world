﻿using System;
using UnityEngine;

[Serializable]
public class CharacterSoundGroup : ICharacterSoundGroup
{
    [SerializeField] private CharacterSoundTypes name;
    [SerializeField] private AudioClip[] sounds;
    [SerializeField] private float volume;

    public AudioClip[] Sounds
    {
        get => sounds;
        set => sounds = value;
    }

    public CharacterSoundTypes Name
    {
        get => name;
        set => name = value;
    }

    public float Volume
    {
        get => volume;
        set => volume = Mathf.Clamp(value, 0, 1);
    }
}