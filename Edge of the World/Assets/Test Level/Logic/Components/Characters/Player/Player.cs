﻿using UnityEngine;

public class Player : Character, IMobile, IPlayer
{
    public Player(GameObject gameObjectRef, ICharacterSoundGroup[] soundGroup) : base(gameObjectRef, soundGroup)
    {
        Camera = CameraUtils.GetPlayerCamera();
        Rigidbody = GameObjectRef.GetComponent<Rigidbody>();

        CanRotate = true;
        CanMove = true;
        CanJump = true;
        CanRun = false;
        IsGrounded = true;
        IsCrouching = false;
    }

    public Camera Camera { get; }
    public Vector3 ShoulderPosition { get; set; }
    public Rigidbody Rigidbody { get; }
    public bool IsInteracting { get; set; }
    public float AirTime { get; set; }
    public float DistanceToGround { get; set; }
    public bool IsGrounded { get; set; }
    public bool IsRunning { get; set; }
    public bool IsCrouching { get; set; }
    public bool CanRotate { get; set; }
    public bool CanMove { get; set; }
    public bool CanRun { get; set; }
    public bool CanJump { get; set; }
}