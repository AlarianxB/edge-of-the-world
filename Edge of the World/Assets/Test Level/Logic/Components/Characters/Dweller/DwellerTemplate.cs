﻿using UnityEngine;

[CreateAssetMenu(fileName = "Dweller", menuName = "Character/Dweller", order = 1)]
public class DwellerTemplate : ScriptableObject
{
    public string dwellerName;
    public bool isInteractable;
    public float triggerZoneRadius;
    public float maxDistanceToPlayer;
}