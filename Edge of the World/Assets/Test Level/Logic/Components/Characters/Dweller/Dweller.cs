﻿using System.Collections.Generic;
using UnityEngine;

public class Dweller: Character, IMobile, IInteractable
{
    private static readonly Dictionary<InteractableActionTypes, string> Actions = new Dictionary<InteractableActionTypes, string>
    {
        {InteractableActionTypes.Talk, "TALK"},
        {InteractableActionTypes.Leave, "LEAVE"}
    }; //TODO move this in utils

    private InteractableActionTypes currentActionType;

    public Dweller(GameObject gameObjectRef, ICharacterSoundGroup[] soundGroup, DwellerTemplate dwellerTemplate) : base(gameObjectRef, soundGroup)
    {

        ObjectCenter = Transform.position;
        TriggerZoneRadius = dwellerTemplate.triggerZoneRadius;
        CanInteractWithPlayer = false;
        CanShowOutline = false;

        LocalizationUtils.Translate("Dwellers", dwellerTemplate.dwellerName, translation => Name = translation);
        CurrentActionType = InteractableActionTypes.Talk;
        IsInteractable = dwellerTemplate.isInteractable;
    }

    public string Name { get; set; }
    public bool IsInteracting { get; set; }
    public bool IsInteractable { get; set; }
    public float DistanceToPlayer { get; set; }
    public bool CanInteractWithPlayer { get; set; }
    public bool CanShowOutline { get; set; }
    public Collider TriggerAreaCollider { get; set; }
    public float TriggerZoneRadius { get; }

    public InteractableActionTypes CurrentActionType
    {
        set
        {
            currentActionType = value;
            LocalizationUtils.Translate("InteractableActions", Actions[value], translation => CurrentActionName = translation);
        }

        get => currentActionType;
    }

    public string CurrentActionName { get; private set; }
    public Vector3 ObjectCenter { get; }

    public float AirTime { get; set; }
    public float DistanceToGround { get; set; }
    public bool IsGrounded { get; set; }
    public bool IsRunning { get; set; }
    public bool IsCrouching { get; set; }
    public bool CanRotate { get; set; }
    public bool CanMove { get; set; }
    public bool CanRun { get; set; }
    public bool CanJump { get; set; }
}