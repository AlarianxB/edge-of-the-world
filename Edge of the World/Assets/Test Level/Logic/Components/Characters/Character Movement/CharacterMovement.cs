﻿using UnityEngine;

public abstract class CharacterMovement
{
    protected static readonly int Movement = Animator.StringToHash("movement");
    protected static readonly int Jump = Animator.StringToHash("jump");
    private static readonly int IsGrounded = Animator.StringToHash("isGrounded");
    private static readonly int IsCrouching = Animator.StringToHash("isCrouching");
    private static readonly int LastAirTime = Animator.StringToHash("lastAirTime");
    private static readonly int AirTime = Animator.StringToHash("airTime");
    private static readonly int IsDirectionalMovement = Animator.StringToHash("isDirectionalMovement");
    private static readonly int DistanceToGround = Animator.StringToHash("distanceToGround");

    private float lastAirTime;

    protected readonly IMobile characterMovementEntity;
    protected readonly ICharacter characterEntity;
    protected readonly CharacterMovementTemplate movementTemplate;

    protected CharacterMovement(ICharacter characterEntity, CharacterMovementTemplate movementTemplate)
    {
        characterMovementEntity = characterEntity as IMobile;
        this.characterEntity = characterEntity;
        this.movementTemplate = movementTemplate;
    }

    public void Update()
    {
        CalculateAirTime();
        characterMovementEntity.IsCrouching = !(characterMovementEntity.AirTime > 0.25f) && characterMovementEntity.IsCrouching;

        SetAnimatorParams();
        if (!characterMovementEntity.IsGrounded) DisableJumpState();
    }
    
    public virtual void FixedUpdate()
    {
        characterMovementEntity.IsGrounded = CheckGrounded();
        GetDistanceToGround();
        SetCharacterRotation();
        MoveCharacter();
    }
    
    public void DisableJumpState()
    {
        characterEntity.Animator.SetBool(Jump, false); //TODO use triggers
    }
    
    public void DisableCrouchState()
    {
        // a special case for jumping in place where you can enable crouching immediately after jumping so we disable crouching in this case 
        characterMovementEntity.IsCrouching = false;
    }
    
    protected virtual void SetAnimatorParams()
    {
        Animator characterAnimator = characterEntity.Animator;
        characterAnimator.SetBool(IsGrounded, characterMovementEntity.IsGrounded);
        characterAnimator.SetBool(IsCrouching, characterMovementEntity.IsCrouching);
        characterAnimator.SetBool(IsDirectionalMovement, AnimationUtils.IsCharacterMoving(characterAnimator));
        characterAnimator.SetFloat(LastAirTime, lastAirTime);
        characterAnimator.SetFloat(AirTime, characterMovementEntity.AirTime);
        characterAnimator.SetFloat(DistanceToGround, characterMovementEntity.DistanceToGround);
    }
    
    private bool CheckGrounded()
    {
        Bounds bounds = characterEntity.Collider.bounds;
        return Physics.SphereCast(bounds.center, 0.2f,
            Vector3.down, out RaycastHit hit, bounds.extents.y, Physics.DefaultRaycastLayers);
    }
    
    private void CalculateAirTime()
    {
        if (characterMovementEntity.IsGrounded)
        {
            if (characterMovementEntity.AirTime > 0f) lastAirTime = characterMovementEntity.AirTime;

            characterMovementEntity.AirTime = 0f;
        }
        else
        {
            characterMovementEntity.AirTime += Time.deltaTime;
        }
    }
    
    private void GetDistanceToGround()
    {
        Bounds bounds = characterEntity.Collider.bounds;
        characterMovementEntity.DistanceToGround = Physics.Raycast(new Vector3(bounds.center.x, bounds.center.y - bounds.extents.y + 0.1f, bounds.center.z),
            Vector3.down, out RaycastHit hit, 10.0f, Physics.DefaultRaycastLayers)
            ? hit.distance
            : 10.0f;
    }
    
    protected float GetSlopeValue()
    {//TODO Test this it seems its not working
        Vector3 playerPosition = characterEntity.Transform.localPosition;

        if (!Physics.Raycast(new Vector3(playerPosition.x, playerPosition.y + 0.3f, playerPosition.z),
            characterEntity.Transform.forward, out RaycastHit hit, 1.0f, Physics.DefaultRaycastLayers) || hit.collider.isTrigger) return 1;
        float dotProduct = Vector3.Dot(Vector3.up, hit.normal);

        if (dotProduct <= 0.05f) // prevent player from getting stuck on vertical surfaces
            return 1;

        return dotProduct;
    }


    protected abstract void SetCharacterRotation();
    protected abstract void MoveCharacter();
}
