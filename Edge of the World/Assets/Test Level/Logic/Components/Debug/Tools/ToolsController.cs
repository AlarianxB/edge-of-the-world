﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using static Cinemachine.CinemachineBrain;

public class ToolsController : MonoBehaviour
{
    //TODO check this file
    private static readonly Dictionary<TestLevelRooms, Vector3> RoomLocations = new Dictionary<TestLevelRooms, Vector3>
    {
        {TestLevelRooms.Movement, new Vector3(14.3999996f, 0.49194622f, -7)},
        {TestLevelRooms.Combat, new Vector3(-10, 0.49194622f, -8.19999981f)},
        {TestLevelRooms.Interactions, new Vector3(-34.9399986f, 0.49194622f, -3.17000008f)},
        {TestLevelRooms.MultiPurpose, new Vector3(4.4000001f, 0.49194622f, 18.7999992f)}
    };

    public Button pauseGameButton;
    public Text pauseGameText;
    public Button enableSlowMotionButton;
    public Text enableSlowMotionText;
    public Button respawnItemsButton;
    public Button resetPlayerButton;
    public Button showTeleportMenuButton;
    public Text showTeleportMenuText;

    public Button movementRoomButton;
    public Button combatRoomButton;
    public Button interactionsRoomButton;
    public Button multiPurposeRoomButton;
    public GameObject teleportMenu;

    private PlayerInputControls inputControls;
    private bool isGamePaused;
    private bool isSlowMotionEnabled;
    private TestLevelRooms lastTestLevelRoom = TestLevelRooms.Interactions;
    private bool isTeleportMenuVisible;
    private ICharacter playerEntity;

    private void Start()
    {
        if (Debug.isDebugBuild)
        {
            playerEntity = GameUtils.GetPlayerEntity();
            SetInputActions();
            SetButtonListeners();
        }
        
        teleportMenu.SetActive(false);
        gameObject.SetActive(false);
    }

    private void Update()
    {
        enableSlowMotionButton.interactable = !isGamePaused;
        pauseGameButton.interactable = !isSlowMotionEnabled;
    }

    private void SetInputActions()
    {
        inputControls = InputService.inputControls;
        inputControls.Debug.PauseGame.performed += _ =>
        {
            if (!isSlowMotionEnabled)
            {
                PauseGame();
            }
        };
        inputControls.Debug.EnableSlowMotion.performed += _ =>
        {
            if (!isGamePaused)
            {
                EnableSlowMotion();
            }
        };
        inputControls.Debug.RespawnItems.performed += _ =>
        {
            if (!((IPlayer)playerEntity).IsInteracting && respawnItemsButton.interactable)
                RespawnItems();
        };
        inputControls.Debug.ResetPlayer.performed += _ => ResetPlayer();
        inputControls.Debug.ShowTeleportMenu.performed += _ => ShowTeleportMenu();
        
        inputControls.Debug.MovementRoom.performed += _ => TeleportPlayer(TestLevelRooms.Movement);
        inputControls.Debug.CombatRoom.performed += _ => TeleportPlayer(TestLevelRooms.Combat);
        inputControls.Debug.InteractionsRoom.performed += _ => TeleportPlayer(TestLevelRooms.Interactions);
        inputControls.Debug.MultiPurposeRoom.performed += _ => TeleportPlayer(TestLevelRooms.MultiPurpose);
    }

    private void SetButtonListeners()
    {
        pauseGameButton.onClick.AddListener(PauseGame);
        enableSlowMotionButton.onClick.AddListener(EnableSlowMotion);
        respawnItemsButton.onClick.AddListener(RespawnItems);
        resetPlayerButton.onClick.AddListener(ResetPlayer);
        showTeleportMenuButton.onClick.AddListener(ShowTeleportMenu);

        movementRoomButton.onClick.AddListener(() => TeleportPlayer(TestLevelRooms.Movement));
        combatRoomButton.onClick.AddListener(() => TeleportPlayer(TestLevelRooms.Combat));
        interactionsRoomButton.onClick.AddListener(() => TeleportPlayer(TestLevelRooms.Interactions));
        multiPurposeRoomButton.onClick.AddListener(() => TeleportPlayer(TestLevelRooms.MultiPurpose));
    }

    private void PauseGame()
    {
        isGamePaused = !isGamePaused;
        Time.timeScale = Convert.ToSingle(!isGamePaused);
        pauseGameText.text = isGamePaused ? "    Resume Game" : "    Pause Game";

        InputSystem.settings.updateMode =
            isGamePaused ? InputSettings.UpdateMode.ProcessEventsInDynamicUpdate : InputSettings.UpdateMode.ProcessEventsInFixedUpdate;

        CameraUtils.GetPlayerCameraBrain().m_UpdateMethod = isGamePaused ? UpdateMethod.LateUpdate : UpdateMethod.FixedUpdate;
        CameraUtils.GetPlayerCameraBrain().m_BlendUpdateMethod = isGamePaused ? BrainUpdateMethod.LateUpdate : BrainUpdateMethod.FixedUpdate;
    }
    
    private void EnableSlowMotion()
    {
        isSlowMotionEnabled = !isSlowMotionEnabled;
        Time.timeScale = isSlowMotionEnabled ? 0.3f : 1;
        enableSlowMotionText.text = isSlowMotionEnabled ? "    Disable Slow Motion" : "    Enable Slow Motion";

        InputSystem.settings.updateMode =
            isSlowMotionEnabled ? InputSettings.UpdateMode.ProcessEventsInDynamicUpdate : InputSettings.UpdateMode.ProcessEventsInFixedUpdate;
    }

    private void RespawnItems()
    {
        DebugService.NotifyEvent(DebugEventTypes.RespawnItems);
    }

    private void ResetPlayer()
    {
        CharacterEnvironmentInteractionService.NotifyEvent(InteractionEventTypes.CharacterTeleport, playerEntity); //TODO use hashes
        playerEntity.Animator.SetBool("pickItemFromGround", false);
        playerEntity.Animator.SetBool("pickItemFromSurface", false);
        playerEntity.Animator.SetBool("sleep", false);
        playerEntity.Animator.SetFloat("handWeight", 0);
        playerEntity.Animator.SetBool("sit", false);
        playerEntity.Animator.SetBool("openDoor", false);
        playerEntity.Animator.SetBool("mirror", false);
        playerEntity.Animator.Play("Directional Movement", -1, 0);
        //TODO move all this to animation util (ResetInteractionAnimations)  
        playerEntity.Transform.position = RoomLocations[lastTestLevelRoom];
    }

    private void ShowTeleportMenu()
    {
        isTeleportMenuVisible = !isTeleportMenuVisible;
        
        showTeleportMenuText.text = isTeleportMenuVisible ? "    Hide Teleport Menu" : "    Show Teleport Menu";
        teleportMenu.SetActive(isTeleportMenuVisible);
    }

    private void TeleportPlayer(TestLevelRooms room)
    {
        lastTestLevelRoom = room;
        ResetPlayer();
    }

    private enum TestLevelRooms
    {
        Movement,
        Combat,
        Interactions,
        MultiPurpose
    }
}