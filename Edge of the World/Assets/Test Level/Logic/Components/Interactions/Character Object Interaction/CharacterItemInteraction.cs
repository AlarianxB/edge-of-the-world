﻿using System.Collections;
using UnityEngine;

public class CharacterItemInteraction : CharacterObjectInteraction
{
    private static readonly int PickItemFromGround = Animator.StringToHash("pickItemFromGround");
    private static readonly int PickItemFromSurface = Animator.StringToHash("pickItemFromSurface");
    private IEnumerator itemInteractionRoutine;
    
    public CharacterItemInteraction(MonoBehaviour monoBehaviourRef, ICharacter characterEntity, IInteractable targetInteractableObject) : base(
        monoBehaviourRef, characterEntity, targetInteractableObject)
    {
    }
    
    public override void Initialize()
    {
        targetIKWeight = 1;
        base.Initialize();
    }
    
    public override void StartInteraction()
    {
        switch (targetInteractableObject.ObjectType)
        {
            case InteractableObjectTypes.GroundItem:
                if (characterMovement.IsRunning)
                {
                    characterMovement.CanRotate = true;
                    characterMovement.CanMove = true;
                }
    
                characterEntity.Animator.SetBool(PickItemFromGround, true);
                break;
            case InteractableObjectTypes.SurfaceItem:
                characterEntity.Animator.SetBool(PickItemFromSurface, true);
                break;
        }
    
        base.StartInteraction();
    }
    
    protected override void OnInteractionEvent(InteractionEventTypes eventType, ICharacter character, IInteractable interactableObject)
    {
        switch (eventType)
        {
            case InteractionEventTypes.ItemInteraction:
                GrabItem();
                break;
            case InteractionEventTypes.AnimationExit:
                FinishInteraction();
                break;
            default:
                return;
        }
    }
    
    protected override IEnumerator GetInitialPositioningMethod()
    {
        return characterMovement.IsRunning && targetInteractableObject.ObjectType == InteractableObjectTypes.GroundItem
            ? null
            : SetCharacterLookRotation();
    }
    
    private void GrabItem()
    {
        targetIKWeight = 0;
        characterEntity.Animator.SetBool(PickItemFromGround, false);
        characterEntity.Animator.SetBool(PickItemFromSurface, false);
        Transform anchorPoint = null;
    
        foreach (Transform child in characterEntity.Animator.GetBoneTransform(HumanBodyBones.RightHand))
            if (child.CompareTag("Anchor"))
                anchorPoint = child;
    
        if (anchorPoint is null) return;
        targetInteractableObject.GameObjectRef.transform.parent = anchorPoint;
        targetInteractableObject.GameObjectRef.transform.localPosition = targetInteractableObject.HandPosition;
        targetInteractableObject.GameObjectRef.transform.localEulerAngles = targetInteractableObject.HandRotation;
    }
    
    public override void FinishInteraction()
    {
        CharacterEnvironmentInteractionService.Event -= OnInteractionEvent;
        characterMovement.CanRotate = true;
        characterMovement.CanMove = true;
        characterMovement.CanJump = true;
    
        if (targetInteractableObject is null) return;
        CharacterEnvironmentInteractionService.EmitterStack.Remove(targetInteractableObject);
        Object.Destroy(targetInteractableObject.GameObjectRef);
        targetInteractableObject = null;
        ((IPlayer)characterEntity).IsInteracting = false;
    }
}