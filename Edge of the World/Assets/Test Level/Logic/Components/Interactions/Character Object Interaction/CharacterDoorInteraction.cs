﻿using System.Collections;
using UnityEngine;

public class CharacterDoorInteraction : CharacterObjectInteraction
{
    private static readonly int OpenDoor = Animator.StringToHash("openDoor");
    
    public CharacterDoorInteraction(MonoBehaviour monoBehaviourRef, ICharacter characterEntity, IInteractable targetInteractableObject) : base(
        monoBehaviourRef, characterEntity, targetInteractableObject)
    {
    }
    
    protected override Vector2 GetCharacterPositionOffset()
    {
        return IsCharacterOnTheOppositeSide() ? new Vector2(0, -1.05f) : base.GetCharacterPositionOffset();
    }
    
    public override void StartInteraction()
    {
        if (targetInteractableObject.ObjectType != InteractableObjectTypes.Door)
            return;
        
        SetCharacterKinematic();
        characterEntity.Animator.SetBool(OpenDoor, true);
    
        base.StartInteraction();
    }
    
    protected override void OnInteractionEvent(InteractionEventTypes eventType, ICharacter character, IInteractable interactableObject)
    {
        switch (eventType)
        {
            case InteractionEventTypes.DoorInteraction:
                monoBehaviourRef.StartCoroutine(OpenDoorInteraction());
                break;
            case InteractionEventTypes.AnimationExit:
                FinishInteraction();
                break;
            default:
                return;
        }
    }
    
    protected override IEnumerator GetInitialPositioningMethod()
    {
        return SetCharacterPosition();
    }
    
    protected override bool GetUseObjectSide() //TODO rename or add comment
    {
        return true;
    }
    
    protected override bool GetEnableCameraRecenter()
    {
        return false;
    }
    
    private IEnumerator OpenDoorInteraction()
    {
        characterEntity.Animator.SetBool(OpenDoor, false);
        targetInteractableObject.Animator.SetBool(OpenDoor, true);
    
        yield return new WaitForSecondsRealtime(0.2f);
        targetInteractableObject.Animator.SetBool(OpenDoor, false);
    }
}