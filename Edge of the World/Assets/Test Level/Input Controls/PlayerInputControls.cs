// GENERATED AUTOMATICALLY FROM 'Assets/Test Level/Input Controls/Input Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInputControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInputControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Input Controls"",
    ""maps"": [
        {
            ""name"": ""Movement"",
            ""id"": ""61080d36-33e3-446d-aa31-be6e84eb8c6f"",
            ""actions"": [
                {
                    ""name"": ""Sprint"",
                    ""type"": ""Button"",
                    ""id"": ""457fd158-61c7-4f84-8d1d-cb49a5fa6c82"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Directional"",
                    ""type"": ""PassThrough"",
                    ""id"": ""befb3cf1-a0dc-472f-ab94-c5b62f93fe5d"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": ""StickDeadzone(max=1)"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""2ff37785-dca8-47a1-b58d-b12f38a4d6b6"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Crouch"",
                    ""type"": ""Button"",
                    ""id"": ""4610808e-bbff-40fe-95fb-3da75c68b20c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""36586812-c5b8-4ec5-a575-05ba2f31fe8a"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""Sprint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d8936e6c-b060-40da-a4c3-d48970902748"",
                    ""path"": ""<XInputController>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Xbox Controller"",
                    ""action"": ""Sprint"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Vertical/Horizontal"",
                    ""id"": ""b492a287-ee67-462c-b8d7-e127b969c4a6"",
                    ""path"": ""2DVector(mode=2)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Directional"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Up"",
                    ""id"": ""73ddf615-ee43-4a64-8252-fd8267aa7744"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""Directional"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Down"",
                    ""id"": ""64c00579-ab1d-41de-afc5-229895bf740f"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""Directional"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Left"",
                    ""id"": ""eb65ba51-ee7e-494e-9874-9264b587d1c6"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""Directional"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Right"",
                    ""id"": ""6d5b6b1a-b561-407a-8b1c-d1a7790e2a75"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""Directional"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""ebf0f1f8-b6ef-4af2-9c8f-03b162bbfc6a"",
                    ""path"": ""<XInputController>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Xbox Controller"",
                    ""action"": ""Directional"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""14296040-dcd4-4234-812f-3b3d28309fc6"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""11a20b18-230f-4651-b763-af9c34f2a693"",
                    ""path"": ""<XInputController>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Xbox Controller"",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""25e38f20-f4c0-49de-9376-24fb62678f68"",
                    ""path"": ""<Keyboard>/c"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""Crouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c0f106ca-8295-4bbd-91c5-35a021f01902"",
                    ""path"": ""<XInputController>/leftStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Xbox Controller"",
                    ""action"": ""Crouch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Camera"",
            ""id"": ""e7b9770d-bfa3-430d-9a3d-52acad2cdfab"",
            ""actions"": [
                {
                    ""name"": ""Look"",
                    ""type"": ""Value"",
                    ""id"": ""671be18a-7764-4fbc-88ed-ff03e0a1ea78"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": ""InvertVector2"",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangeShoulderSide"",
                    ""type"": ""Button"",
                    ""id"": ""f7174bf4-0cfe-4ff2-8a78-54837f7cd80d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": ""InvertVector2(invertX=false,invertY=false)"",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""abe72d41-979d-45ad-8098-963261aef7c2"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""47c26e9a-0918-4c45-bcee-5a62c2581bc4"",
                    ""path"": ""<XInputController>/rightStick"",
                    ""interactions"": """",
                    ""processors"": ""ScaleVector2(x=10,y=10)"",
                    ""groups"": ""Xbox Controller"",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""27dc12d7-d923-4ace-9cae-4243d94d417d"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""ChangeShoulderSide"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b40ad8b5-f798-4c84-9af9-c3d3c833d38e"",
                    ""path"": ""<XInputController>/rightStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Xbox Controller"",
                    ""action"": ""ChangeShoulderSide"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Interaction"",
            ""id"": ""6cd7d145-b955-4c2f-8bad-29e6ace342e3"",
            ""actions"": [
                {
                    ""name"": ""Interact"",
                    ""type"": ""Button"",
                    ""id"": ""7cb5f16f-7288-42a1-bac7-b84e7b148017"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""64a3e21a-bb97-451a-acee-0b1311452c79"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d6d0d81b-1427-454e-a1b1-aa5f857cf70d"",
                    ""path"": ""<XInputController>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Xbox Controller"",
                    ""action"": ""Interact"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Debug"",
            ""id"": ""a972218d-1b07-4bf3-9cb7-f5376a8f4a3e"",
            ""actions"": [
                {
                    ""name"": ""ShowSystemInfo"",
                    ""type"": ""Button"",
                    ""id"": ""38b56142-1741-43d1-b40b-d65b554b32e1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ShowMouse"",
                    ""type"": ""Button"",
                    ""id"": ""c580d835-ee42-4ba8-ba78-6994f929bec7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ShowTools"",
                    ""type"": ""Button"",
                    ""id"": ""308f521b-9acc-4f44-9d98-7b50ece67987"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ExitGame"",
                    ""type"": ""Button"",
                    ""id"": ""c67ed480-35ee-4613-af61-523ed7acfc9b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PauseGame"",
                    ""type"": ""Button"",
                    ""id"": ""854588b1-73a7-4836-af14-26f32dfc1793"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""EnableSlowMotion"",
                    ""type"": ""Button"",
                    ""id"": ""e0fdd178-6250-4b23-afa6-652e61d5bb72"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ResetPlayer"",
                    ""type"": ""Button"",
                    ""id"": ""c0a6044d-1fd9-4448-8cc6-9eb39ff3f65c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ShowTeleportMenu"",
                    ""type"": ""Button"",
                    ""id"": ""3c7eb65f-48ce-4162-b9db-65e29757942f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RespawnItems"",
                    ""type"": ""Button"",
                    ""id"": ""8cce7b66-510f-4eb8-b19a-67ca185efb55"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MovementRoom"",
                    ""type"": ""Button"",
                    ""id"": ""b1e7eb96-6b5d-4758-92df-023339fbf28d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""CombatRoom"",
                    ""type"": ""Button"",
                    ""id"": ""b8071f2d-0e3b-485f-83a0-5b3c97ef6eed"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""InteractionsRoom"",
                    ""type"": ""Button"",
                    ""id"": ""2c722c24-f2d6-4746-a1f9-3a7adfaec6de"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MultiPurposeRoom"",
                    ""type"": ""Button"",
                    ""id"": ""ba758637-617b-4c8d-a86b-cdf8e05f09eb"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""TakeScreenshot"",
                    ""type"": ""Button"",
                    ""id"": ""3bdd9487-9acd-4461-8d7c-c1c49e53bab0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""3884bbdd-e564-4b2c-97bd-0e9ecb7e0ec0"",
                    ""path"": ""<Keyboard>/f2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""ShowSystemInfo"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6b7ec497-010e-49ff-b5b4-2b3eaa8ed7f8"",
                    ""path"": ""<Keyboard>/f1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""ShowMouse"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c5bd5138-070b-4714-b0b6-63b6bcd94cb8"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""ExitGame"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c8b6f2ca-d21e-4ba0-b8e9-e7242c6f738e"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""PauseGame"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""21a8f798-1f5e-4caf-98ba-954377bdf554"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""ResetPlayer"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""de0d84e2-8bfb-496f-805b-e2ca5dad9198"",
                    ""path"": ""<Keyboard>/t"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""ShowTeleportMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""973df1ad-e071-40ed-bd8f-c934fb96ff54"",
                    ""path"": ""<Keyboard>/i"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""RespawnItems"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d8896e34-dc65-45be-9b93-7f0b2db08f70"",
                    ""path"": ""<Keyboard>/m"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""EnableSlowMotion"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f96f22a2-fcfa-4c3a-880d-5ba71c3180a4"",
                    ""path"": ""<Keyboard>/f3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""ShowTools"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""045d0993-ecfa-4e96-9235-9364f73f1748"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""MovementRoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b40b407b-ea3c-407e-9bb9-2e690d09fcb4"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""CombatRoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1d8d04c7-9059-4081-aae0-670df0cef93f"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""InteractionsRoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""73e426b7-29dc-49b9-8c4f-0aaa37c8e55d"",
                    ""path"": ""<Keyboard>/4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""MultiPurposeRoom"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1b67ae03-de77-458f-a741-6df2b8e859a0"",
                    ""path"": ""<Keyboard>/f4"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""TakeScreenshot"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard and Mouse"",
            ""bindingGroup"": ""Keyboard and Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": true,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Xbox Controller"",
            ""bindingGroup"": ""Xbox Controller"",
            ""devices"": [
                {
                    ""devicePath"": ""<XInputController>"",
                    ""isOptional"": true,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Movement
        m_Movement = asset.FindActionMap("Movement", throwIfNotFound: true);
        m_Movement_Sprint = m_Movement.FindAction("Sprint", throwIfNotFound: true);
        m_Movement_Directional = m_Movement.FindAction("Directional", throwIfNotFound: true);
        m_Movement_Jump = m_Movement.FindAction("Jump", throwIfNotFound: true);
        m_Movement_Crouch = m_Movement.FindAction("Crouch", throwIfNotFound: true);
        // Camera
        m_Camera = asset.FindActionMap("Camera", throwIfNotFound: true);
        m_Camera_Look = m_Camera.FindAction("Look", throwIfNotFound: true);
        m_Camera_ChangeShoulderSide = m_Camera.FindAction("ChangeShoulderSide", throwIfNotFound: true);
        // Interaction
        m_Interaction = asset.FindActionMap("Interaction", throwIfNotFound: true);
        m_Interaction_Interact = m_Interaction.FindAction("Interact", throwIfNotFound: true);
        // Debug
        m_Debug = asset.FindActionMap("Debug", throwIfNotFound: true);
        m_Debug_ShowSystemInfo = m_Debug.FindAction("ShowSystemInfo", throwIfNotFound: true);
        m_Debug_ShowMouse = m_Debug.FindAction("ShowMouse", throwIfNotFound: true);
        m_Debug_ShowTools = m_Debug.FindAction("ShowTools", throwIfNotFound: true);
        m_Debug_ExitGame = m_Debug.FindAction("ExitGame", throwIfNotFound: true);
        m_Debug_PauseGame = m_Debug.FindAction("PauseGame", throwIfNotFound: true);
        m_Debug_EnableSlowMotion = m_Debug.FindAction("EnableSlowMotion", throwIfNotFound: true);
        m_Debug_ResetPlayer = m_Debug.FindAction("ResetPlayer", throwIfNotFound: true);
        m_Debug_ShowTeleportMenu = m_Debug.FindAction("ShowTeleportMenu", throwIfNotFound: true);
        m_Debug_RespawnItems = m_Debug.FindAction("RespawnItems", throwIfNotFound: true);
        m_Debug_MovementRoom = m_Debug.FindAction("MovementRoom", throwIfNotFound: true);
        m_Debug_CombatRoom = m_Debug.FindAction("CombatRoom", throwIfNotFound: true);
        m_Debug_InteractionsRoom = m_Debug.FindAction("InteractionsRoom", throwIfNotFound: true);
        m_Debug_MultiPurposeRoom = m_Debug.FindAction("MultiPurposeRoom", throwIfNotFound: true);
        m_Debug_TakeScreenshot = m_Debug.FindAction("TakeScreenshot", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Movement
    private readonly InputActionMap m_Movement;
    private IMovementActions m_MovementActionsCallbackInterface;
    private readonly InputAction m_Movement_Sprint;
    private readonly InputAction m_Movement_Directional;
    private readonly InputAction m_Movement_Jump;
    private readonly InputAction m_Movement_Crouch;
    public struct MovementActions
    {
        private @PlayerInputControls m_Wrapper;
        public MovementActions(@PlayerInputControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Sprint => m_Wrapper.m_Movement_Sprint;
        public InputAction @Directional => m_Wrapper.m_Movement_Directional;
        public InputAction @Jump => m_Wrapper.m_Movement_Jump;
        public InputAction @Crouch => m_Wrapper.m_Movement_Crouch;
        public InputActionMap Get() { return m_Wrapper.m_Movement; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MovementActions set) { return set.Get(); }
        public void SetCallbacks(IMovementActions instance)
        {
            if (m_Wrapper.m_MovementActionsCallbackInterface != null)
            {
                @Sprint.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnSprint;
                @Sprint.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnSprint;
                @Sprint.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnSprint;
                @Directional.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnDirectional;
                @Directional.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnDirectional;
                @Directional.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnDirectional;
                @Jump.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnJump;
                @Crouch.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnCrouch;
                @Crouch.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnCrouch;
                @Crouch.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnCrouch;
            }
            m_Wrapper.m_MovementActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Sprint.started += instance.OnSprint;
                @Sprint.performed += instance.OnSprint;
                @Sprint.canceled += instance.OnSprint;
                @Directional.started += instance.OnDirectional;
                @Directional.performed += instance.OnDirectional;
                @Directional.canceled += instance.OnDirectional;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Crouch.started += instance.OnCrouch;
                @Crouch.performed += instance.OnCrouch;
                @Crouch.canceled += instance.OnCrouch;
            }
        }
    }
    public MovementActions @Movement => new MovementActions(this);

    // Camera
    private readonly InputActionMap m_Camera;
    private ICameraActions m_CameraActionsCallbackInterface;
    private readonly InputAction m_Camera_Look;
    private readonly InputAction m_Camera_ChangeShoulderSide;
    public struct CameraActions
    {
        private @PlayerInputControls m_Wrapper;
        public CameraActions(@PlayerInputControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Look => m_Wrapper.m_Camera_Look;
        public InputAction @ChangeShoulderSide => m_Wrapper.m_Camera_ChangeShoulderSide;
        public InputActionMap Get() { return m_Wrapper.m_Camera; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CameraActions set) { return set.Get(); }
        public void SetCallbacks(ICameraActions instance)
        {
            if (m_Wrapper.m_CameraActionsCallbackInterface != null)
            {
                @Look.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnLook;
                @ChangeShoulderSide.started -= m_Wrapper.m_CameraActionsCallbackInterface.OnChangeShoulderSide;
                @ChangeShoulderSide.performed -= m_Wrapper.m_CameraActionsCallbackInterface.OnChangeShoulderSide;
                @ChangeShoulderSide.canceled -= m_Wrapper.m_CameraActionsCallbackInterface.OnChangeShoulderSide;
            }
            m_Wrapper.m_CameraActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
                @ChangeShoulderSide.started += instance.OnChangeShoulderSide;
                @ChangeShoulderSide.performed += instance.OnChangeShoulderSide;
                @ChangeShoulderSide.canceled += instance.OnChangeShoulderSide;
            }
        }
    }
    public CameraActions @Camera => new CameraActions(this);

    // Interaction
    private readonly InputActionMap m_Interaction;
    private IInteractionActions m_InteractionActionsCallbackInterface;
    private readonly InputAction m_Interaction_Interact;
    public struct InteractionActions
    {
        private @PlayerInputControls m_Wrapper;
        public InteractionActions(@PlayerInputControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Interact => m_Wrapper.m_Interaction_Interact;
        public InputActionMap Get() { return m_Wrapper.m_Interaction; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(InteractionActions set) { return set.Get(); }
        public void SetCallbacks(IInteractionActions instance)
        {
            if (m_Wrapper.m_InteractionActionsCallbackInterface != null)
            {
                @Interact.started -= m_Wrapper.m_InteractionActionsCallbackInterface.OnInteract;
                @Interact.performed -= m_Wrapper.m_InteractionActionsCallbackInterface.OnInteract;
                @Interact.canceled -= m_Wrapper.m_InteractionActionsCallbackInterface.OnInteract;
            }
            m_Wrapper.m_InteractionActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Interact.started += instance.OnInteract;
                @Interact.performed += instance.OnInteract;
                @Interact.canceled += instance.OnInteract;
            }
        }
    }
    public InteractionActions @Interaction => new InteractionActions(this);

    // Debug
    private readonly InputActionMap m_Debug;
    private IDebugActions m_DebugActionsCallbackInterface;
    private readonly InputAction m_Debug_ShowSystemInfo;
    private readonly InputAction m_Debug_ShowMouse;
    private readonly InputAction m_Debug_ShowTools;
    private readonly InputAction m_Debug_ExitGame;
    private readonly InputAction m_Debug_PauseGame;
    private readonly InputAction m_Debug_EnableSlowMotion;
    private readonly InputAction m_Debug_ResetPlayer;
    private readonly InputAction m_Debug_ShowTeleportMenu;
    private readonly InputAction m_Debug_RespawnItems;
    private readonly InputAction m_Debug_MovementRoom;
    private readonly InputAction m_Debug_CombatRoom;
    private readonly InputAction m_Debug_InteractionsRoom;
    private readonly InputAction m_Debug_MultiPurposeRoom;
    private readonly InputAction m_Debug_TakeScreenshot;
    public struct DebugActions
    {
        private @PlayerInputControls m_Wrapper;
        public DebugActions(@PlayerInputControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @ShowSystemInfo => m_Wrapper.m_Debug_ShowSystemInfo;
        public InputAction @ShowMouse => m_Wrapper.m_Debug_ShowMouse;
        public InputAction @ShowTools => m_Wrapper.m_Debug_ShowTools;
        public InputAction @ExitGame => m_Wrapper.m_Debug_ExitGame;
        public InputAction @PauseGame => m_Wrapper.m_Debug_PauseGame;
        public InputAction @EnableSlowMotion => m_Wrapper.m_Debug_EnableSlowMotion;
        public InputAction @ResetPlayer => m_Wrapper.m_Debug_ResetPlayer;
        public InputAction @ShowTeleportMenu => m_Wrapper.m_Debug_ShowTeleportMenu;
        public InputAction @RespawnItems => m_Wrapper.m_Debug_RespawnItems;
        public InputAction @MovementRoom => m_Wrapper.m_Debug_MovementRoom;
        public InputAction @CombatRoom => m_Wrapper.m_Debug_CombatRoom;
        public InputAction @InteractionsRoom => m_Wrapper.m_Debug_InteractionsRoom;
        public InputAction @MultiPurposeRoom => m_Wrapper.m_Debug_MultiPurposeRoom;
        public InputAction @TakeScreenshot => m_Wrapper.m_Debug_TakeScreenshot;
        public InputActionMap Get() { return m_Wrapper.m_Debug; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(DebugActions set) { return set.Get(); }
        public void SetCallbacks(IDebugActions instance)
        {
            if (m_Wrapper.m_DebugActionsCallbackInterface != null)
            {
                @ShowSystemInfo.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowSystemInfo;
                @ShowSystemInfo.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowSystemInfo;
                @ShowSystemInfo.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowSystemInfo;
                @ShowMouse.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowMouse;
                @ShowMouse.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowMouse;
                @ShowMouse.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowMouse;
                @ShowTools.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowTools;
                @ShowTools.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowTools;
                @ShowTools.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowTools;
                @ExitGame.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnExitGame;
                @ExitGame.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnExitGame;
                @ExitGame.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnExitGame;
                @PauseGame.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnPauseGame;
                @PauseGame.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnPauseGame;
                @PauseGame.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnPauseGame;
                @EnableSlowMotion.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnEnableSlowMotion;
                @EnableSlowMotion.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnEnableSlowMotion;
                @EnableSlowMotion.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnEnableSlowMotion;
                @ResetPlayer.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnResetPlayer;
                @ResetPlayer.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnResetPlayer;
                @ResetPlayer.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnResetPlayer;
                @ShowTeleportMenu.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowTeleportMenu;
                @ShowTeleportMenu.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowTeleportMenu;
                @ShowTeleportMenu.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnShowTeleportMenu;
                @RespawnItems.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnRespawnItems;
                @RespawnItems.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnRespawnItems;
                @RespawnItems.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnRespawnItems;
                @MovementRoom.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnMovementRoom;
                @MovementRoom.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnMovementRoom;
                @MovementRoom.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnMovementRoom;
                @CombatRoom.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnCombatRoom;
                @CombatRoom.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnCombatRoom;
                @CombatRoom.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnCombatRoom;
                @InteractionsRoom.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnInteractionsRoom;
                @InteractionsRoom.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnInteractionsRoom;
                @InteractionsRoom.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnInteractionsRoom;
                @MultiPurposeRoom.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnMultiPurposeRoom;
                @MultiPurposeRoom.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnMultiPurposeRoom;
                @MultiPurposeRoom.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnMultiPurposeRoom;
                @TakeScreenshot.started -= m_Wrapper.m_DebugActionsCallbackInterface.OnTakeScreenshot;
                @TakeScreenshot.performed -= m_Wrapper.m_DebugActionsCallbackInterface.OnTakeScreenshot;
                @TakeScreenshot.canceled -= m_Wrapper.m_DebugActionsCallbackInterface.OnTakeScreenshot;
            }
            m_Wrapper.m_DebugActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ShowSystemInfo.started += instance.OnShowSystemInfo;
                @ShowSystemInfo.performed += instance.OnShowSystemInfo;
                @ShowSystemInfo.canceled += instance.OnShowSystemInfo;
                @ShowMouse.started += instance.OnShowMouse;
                @ShowMouse.performed += instance.OnShowMouse;
                @ShowMouse.canceled += instance.OnShowMouse;
                @ShowTools.started += instance.OnShowTools;
                @ShowTools.performed += instance.OnShowTools;
                @ShowTools.canceled += instance.OnShowTools;
                @ExitGame.started += instance.OnExitGame;
                @ExitGame.performed += instance.OnExitGame;
                @ExitGame.canceled += instance.OnExitGame;
                @PauseGame.started += instance.OnPauseGame;
                @PauseGame.performed += instance.OnPauseGame;
                @PauseGame.canceled += instance.OnPauseGame;
                @EnableSlowMotion.started += instance.OnEnableSlowMotion;
                @EnableSlowMotion.performed += instance.OnEnableSlowMotion;
                @EnableSlowMotion.canceled += instance.OnEnableSlowMotion;
                @ResetPlayer.started += instance.OnResetPlayer;
                @ResetPlayer.performed += instance.OnResetPlayer;
                @ResetPlayer.canceled += instance.OnResetPlayer;
                @ShowTeleportMenu.started += instance.OnShowTeleportMenu;
                @ShowTeleportMenu.performed += instance.OnShowTeleportMenu;
                @ShowTeleportMenu.canceled += instance.OnShowTeleportMenu;
                @RespawnItems.started += instance.OnRespawnItems;
                @RespawnItems.performed += instance.OnRespawnItems;
                @RespawnItems.canceled += instance.OnRespawnItems;
                @MovementRoom.started += instance.OnMovementRoom;
                @MovementRoom.performed += instance.OnMovementRoom;
                @MovementRoom.canceled += instance.OnMovementRoom;
                @CombatRoom.started += instance.OnCombatRoom;
                @CombatRoom.performed += instance.OnCombatRoom;
                @CombatRoom.canceled += instance.OnCombatRoom;
                @InteractionsRoom.started += instance.OnInteractionsRoom;
                @InteractionsRoom.performed += instance.OnInteractionsRoom;
                @InteractionsRoom.canceled += instance.OnInteractionsRoom;
                @MultiPurposeRoom.started += instance.OnMultiPurposeRoom;
                @MultiPurposeRoom.performed += instance.OnMultiPurposeRoom;
                @MultiPurposeRoom.canceled += instance.OnMultiPurposeRoom;
                @TakeScreenshot.started += instance.OnTakeScreenshot;
                @TakeScreenshot.performed += instance.OnTakeScreenshot;
                @TakeScreenshot.canceled += instance.OnTakeScreenshot;
            }
        }
    }
    public DebugActions @Debug => new DebugActions(this);
    private int m_KeyboardandMouseSchemeIndex = -1;
    public InputControlScheme KeyboardandMouseScheme
    {
        get
        {
            if (m_KeyboardandMouseSchemeIndex == -1) m_KeyboardandMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard and Mouse");
            return asset.controlSchemes[m_KeyboardandMouseSchemeIndex];
        }
    }
    private int m_XboxControllerSchemeIndex = -1;
    public InputControlScheme XboxControllerScheme
    {
        get
        {
            if (m_XboxControllerSchemeIndex == -1) m_XboxControllerSchemeIndex = asset.FindControlSchemeIndex("Xbox Controller");
            return asset.controlSchemes[m_XboxControllerSchemeIndex];
        }
    }
    public interface IMovementActions
    {
        void OnSprint(InputAction.CallbackContext context);
        void OnDirectional(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnCrouch(InputAction.CallbackContext context);
    }
    public interface ICameraActions
    {
        void OnLook(InputAction.CallbackContext context);
        void OnChangeShoulderSide(InputAction.CallbackContext context);
    }
    public interface IInteractionActions
    {
        void OnInteract(InputAction.CallbackContext context);
    }
    public interface IDebugActions
    {
        void OnShowSystemInfo(InputAction.CallbackContext context);
        void OnShowMouse(InputAction.CallbackContext context);
        void OnShowTools(InputAction.CallbackContext context);
        void OnExitGame(InputAction.CallbackContext context);
        void OnPauseGame(InputAction.CallbackContext context);
        void OnEnableSlowMotion(InputAction.CallbackContext context);
        void OnResetPlayer(InputAction.CallbackContext context);
        void OnShowTeleportMenu(InputAction.CallbackContext context);
        void OnRespawnItems(InputAction.CallbackContext context);
        void OnMovementRoom(InputAction.CallbackContext context);
        void OnCombatRoom(InputAction.CallbackContext context);
        void OnInteractionsRoom(InputAction.CallbackContext context);
        void OnMultiPurposeRoom(InputAction.CallbackContext context);
        void OnTakeScreenshot(InputAction.CallbackContext context);
    }
}
