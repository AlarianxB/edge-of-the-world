﻿using UnityEditor;

internal class GameBuilder
{
    private static void PerformBuild()
    {
        string[] scenes = {"Assets/Test Level/Scenes/Main Test Level.unity"};
        BuildPipeline.BuildPlayer(scenes, "H:/Unity Projects/Edge of the World - Builds/Edge of the World.exe", BuildTarget.StandaloneWindows64,
            BuildOptions.Development);
    }
}